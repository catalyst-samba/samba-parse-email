# samba-parse-email

The `parse-autobuild-email` script reads a mailbox containing emails
from the samba-cvs list, and fetches the remote logs referenced in
those emails. It can filter and process those logs in various ways to
report on the ways in which samba autobuilds on sn-devel are failing.

If not many of those words mean much to you, this is not the repo for
you. In the unlikely event that you are still interested, use 
`parse-autobuild-email --help` to find out more.

## History

This was originally developed as part of Catalyst IT's
samba-cloud-autobuild repository. It gained independence in June 2019
thanks to git filter-branch.
